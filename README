
PLINK (version 1.08) is a free, open-source whole genome association
analysis toolset, designed to perform a range of basic, large-scale
analyses in a computationally efficient manner.

The focus of PLINK is purely on analysis of genotype/phenotype data,
so there is no support for steps prior to this (e.g. study design and
planning, generating genotype or CNV calls from raw data). Through
integration with gPLINK and Haploview, there is some support for the
subsequent visualization, annotation and storage of results.

PLINK (one syllable) was developed by Shaun Purcell whilst at the
Center for Human Genetic Research (CHGR), Massachusetts General
Hospital (MGH), and the Broad Institute of Harvard & MIT, with the
support of others.

For more information, please visit:

     http://zzz.bwh.harvard.edu/plink/


To compile, edit the first few lines of the Makefile as appropriate (options 
described in the Makefile, should be self-explanatory -- the most important 
one is set SYS to your operating system (e.g. primarily MAC or UNIX).  Then type
  
    make

and an executable file 'plink' should be generated.  A library file
(dcdflib) may generate a lot of warnings when compiling, which can be
ignored.


A newer rewrite of PLINK ("version 1.90", to become PLINK2) has been
developed by Chris Chang has a number of new features and significant
improvement in performance for large datasets.  This is freely
available for beta-release download at:

     https://www.cog-genomics.org/plink2

